DROP TABLE IF EXISTS `aclresources`;

CREATE TABLE `aclresources` (
  `rsid` int(10) NOT NULL AUTO_INCREMENT,
  `access` varchar(255) NOT NULL,
  `aclcontrol` varchar(200) NOT NULL,
  `aclaction` varchar(200) NOT NULL,
  `name` varchar(255) NOT NULL,
  `createdtime` varchar(255) NOT NULL,
  `updatedtime` varchar(255) NOT NULL,
  PRIMARY KEY (`rsid`),
  KEY `control` (`aclcontrol`),
  KEY `action` (`aclaction`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `aclresources` */

insert  into `aclresources`(`rsid`,`access`,`aclcontrol`,`aclaction`,`name`,`createdtime`,`updatedtime`) values (1,'1','frontend_do','login','登录','',''),(2,'1','frontend_do','dashboard','仪表盘','',''),(3,'2','frontend_operation','list','作业列表','',''),(4,'2','frontend_operation','add','添加作业','',''),(5,'2','frontend_operation','update','更新作业','',''),(6,'2','frontend_organization','adduser','添加用户','',''),(7,'2','frontend_organization','updateuser','更新用户','',''),(8,'2','frontend_organization','listuser','用户列表','',''),(9,'2','frontend_organization','list','组织结构列表','',''),(10,'2','frontend_organization','add','添加组织结构','',''),(11,'2','frontend_organization','update','更新组织结构','',''),(12,'2','frontend_test','index','测试','',''),(13,'2','frontend_former','list','模型列表','',''),(14,'2','frontend_former','add','添加模型','',''),(15,'2','frontend_former','update','更新模型','',''),(16,'2','frontend_former','listprototype','原型列表','',''),(17,'2','frontend_former','addprototype','增加原型','',''),(18,'2','frontend_former','updateprototype','更新原型','',''),(19,'2','frontend_former','listfield','字段列表','',''),(20,'2','frontend_former','addfield','增加字段','',''),(21,'2','frontend_former','updatefield','更新字段','',''),(22,'1','frontend_do','mns','消息通知','',''),(23,'1','frontend_do','getdata','异步获取数据','',''),(24,'2','frontend_operation','workflow','作业工作流','',''),(25,'2','frontend_do','logout','退出','',''),(26,'2','frontend_do','updatepassword','修改密码','',''),(27,'2','frontend_former','listworkflow','工作流列表','',''),(28,'2','frontend_former','addworkflow','添加工作流','',''),(29,'2','frontend_former','updateworkflow','更新工作流','',''),(30,'2','frontend_do','logs','操作日志','',''),(31,'2','frontend_do','setting','系统配置','','');

/*Table structure for table `aclresourcesroles` */

DROP TABLE IF EXISTS `aclresourcesroles`;

CREATE TABLE `aclresourcesroles` (
  `rsid` int(10) NOT NULL,
  `roleid` int(10) NOT NULL,
  KEY `roleid` (`roleid`),
  KEY `rsid` (`rsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `aclresourcesroles` */

insert  into `aclresourcesroles`(`rsid`,`roleid`) values (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1);

/*Table structure for table `aclroles` */

DROP TABLE IF EXISTS `aclroles`;

CREATE TABLE `aclroles` (
  `rid` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `createdtime` varchar(255) NOT NULL,
  `updatedtime` varchar(255) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `aclroles` */

insert  into `aclroles`(`rid`,`name`,`intro`,`createdtime`,`updatedtime`) values (1,'管理员','系统管理员','','1364629048'),(2,'员工','查看和录入','','');

/*Table structure for table `fields` */

DROP TABLE IF EXISTS `fields`;

CREATE TABLE `fields` (
  `fieldid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` text,
  `sort` varchar(20) NOT NULL,
  `defaultvalue` text,
  `options` text,
  `formtype` varchar(20) NOT NULL DEFAULT '',
  `inputtool` varchar(20) NOT NULL DEFAULT '',
  `inputlimit` varchar(20) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `enablesearch` tinyint(1) NOT NULL DEFAULT '0',
  `showinlist` tinyint(1) NOT NULL DEFAULT '1',
  `prototypeid` int(10) NOT NULL DEFAULT '0',
  `bindingid` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fieldid`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `formers` */

DROP TABLE IF EXISTS `formers`;

CREATE TABLE `formers` (
  `formerid` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `setting` text,
  `desc` varchar(200) DEFAULT NULL,
  `uid` int(10) NOT NULL DEFAULT '0',
  `enable` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`formerid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `groupid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `roles` varchar(200) NOT NULL,
  `uid` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `groups` */

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `logid` int(10) NOT NULL AUTO_INCREMENT,
  `logDate` varchar(100) NOT NULL,
  `logTime` varchar(100) NOT NULL,
  `logIp` varchar(100) NOT NULL,
  `logString` text NOT NULL,
  PRIMARY KEY (`logid`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8;

/*Data for the table `logs` */

/*Table structure for table `organizations` */

DROP TABLE IF EXISTS `organizations`;

CREATE TABLE `organizations` (
  `organizationid` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `users` varchar(200) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`organizationid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `organizations` */

/*Table structure for table `prototypes` */

DROP TABLE IF EXISTS `prototypes`;

CREATE TABLE `prototypes` (
  `prototypeid` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `setting` text,
  `formerid` int(10) NOT NULL DEFAULT '0',
  `desc` varchar(200) DEFAULT NULL,
  `dashboard` tinyint(1) NOT NULL DEFAULT '1',
  `workflowable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`prototypeid`),
  UNIQUE KEY `modelid` (`prototypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `prototypes` */

/*Table structure for table `regs` */

DROP TABLE IF EXISTS `regs`;

CREATE TABLE `regs` (
  `regid` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `servicename` varchar(200) NOT NULL,
  `serviceid` int(10) NOT NULL,
  `prototypeid` int(10) NOT NULL,
  `setting` text NOT NULL,
  PRIMARY KEY (`regid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `regs` */

/*Table structure for table `sessions` */

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `sessionid` varchar(750) DEFAULT NULL,
  `data` varchar(750) DEFAULT NULL,
  `lastvisit` varchar(600) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sessions` */

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `skey` varchar(100) NOT NULL,
  `svalue` varchar(255) NOT NULL,
  `intro` varchar(200) NOT NULL,
  PRIMARY KEY (`skey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `settings` */

insert  into `settings`(`skey`,`svalue`,`intro`) values ('copyright',' 2016 ','版权'),('emailbase','','账号默认后缀'),('siteCount','11111111','网站访问量'),('sysname','运营系统','系统名称'),('sysshortname','OMS','系统简称'),('whitelist','127.0.0.1','白名单');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth` varchar(50) NOT NULL,
  `organizations` varchar(255) DEFAULT NULL,
  `telnum` varchar(255) DEFAULT NULL,
  `truename` varchar(100) NOT NULL,
  `settings` varchar(255) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `roleids` varchar(255) NOT NULL,
  `createtime` varchar(200) NOT NULL,
  `updatetime` varchar(200) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`uid`,`username`,`email`,`password`,`auth`,`organizations`,`telnum`,`truename`,`settings`,`title`,`roleids`,`createtime`,`updatetime`) values (1,'murray','murray@xxx.com','e0a4f241df92b75fa4b5a4482d281e89','UULxaQ5NpZYLggrmeeTZGCHVRqE8pJd0','5,3,2,1','186xxxxxx','Murray',NULL,'产品','1','1468170527','1471306640');

/*Table structure for table `workflows` */

DROP TABLE IF EXISTS `workflows`;

CREATE TABLE `workflows` (
  `workflowid` int(10) NOT NULL AUTO_INCREMENT,
  `keyname` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `before` varchar(100) NOT NULL,
  `after` varchar(100) NOT NULL,
  `uid` int(10) NOT NULL,
  `organizationid` int(10) NOT NULL,
  `roleid` int(10) NOT NULL,
  `fieldshow` varchar(255) NOT NULL,
  `fieldhidden` varchar(255) NOT NULL,
  `prototypeid` int(10) NOT NULL,
  PRIMARY KEY (`workflowid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `workflows` */